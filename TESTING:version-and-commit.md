- [ng-apps](#ng-apps)


## ng-apps
| Deployment | Image | Commit | Timestamp |
|------------|-------|--------|-----------|
| [wp Project](https://gitlab.com/ssss12/terraform/-/tree/main) | beta-6.4-RC3-fpm | [85c90999](https://gitlab.com/ssss12/terraform/-/commit/85c909997e43e6e3dd98f4734ed3915283632094) | 2023-11-21 13:58:42 |
| [novydeploy Project](https://gitlab.com/ssss12/novydeploy/-/tree/main) | 1.24.0 | [608f5c62](https://gitlab.com/ssss12/novydeploy/-/commit/608f5c62d96070cf9384afd696bfa162f00bc047) | 2023-11-29 17:27:22 |
| [nginx Project](https://gitlab.com/ssss12/microservice/-/tree/main) | 1.25.3-perl | [9c81fca2](https://gitlab.com/ssss12/microservice/-/commit/9c81fca2e2c04ffbbed78f17086443d7ccfa3ab1) | 2023-11-30 13:00:13 |
| [redis Project](https://gitlab.com/ssss12/nginx1/-/tree/main) | 5 | [421078f9](https://gitlab.com/ssss12/nginx1/-/commit/421078f909fa9ad1f6f58ac81bb4e43fd80e17d4) | 2023-11-30 14:13:28 |
